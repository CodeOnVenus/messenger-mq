<?php

namespace App\Message;

class MailNotification {

    private $description;
    private $id;
    private $from;

    public function __construct(
        string $description,
        int $id,
        string $from
    )
    {
        $this->description = $description;
        $this->id = $id;
        $this->from = $from;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getFrom()
    {
        return $this->from;
    }
}
