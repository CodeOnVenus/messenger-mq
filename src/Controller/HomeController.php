<?php

namespace App\Controller;

use App\Entity\FeedBack;
use App\Form\FeedBackType;
use App\Message\MailNotification;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/home', name: 'app_home')]
    public function index(
        EntityManagerInterface $em,
        Request $request,
        MessageBusInterface $bus): Response
    {

        $feedback = new FeedBack();
        $feedback->setUser($this->getUser())
                    ->setCreatedAt(new DateTime('now'));
        $form = $this->createForm(FeedBackType::class, $feedback);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $feedback = $form->getData();

            $em->persist($feedback);
            $em->flush();

            $bus->dispatch(new MailNotification(
                $feedback->getDescription(), 
                $feedback->getId(), 
                $feedback->getUser()->getEmail()));

            return $this->redirectToRoute('app_home');

        }

        return $this->render('home/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
